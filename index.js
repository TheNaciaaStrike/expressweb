const express = require('express')
const bodyParser = require('body-parser')
var device = require('express-device');
const cors = require('cors')
const path = require('path');


const app = express()
const port = 3000
app.use(device.capture());


let tasklist = [{
    "id": "1",
    "pavadinimas": "test",
    "terminas": "2020-12-20",
    "ikelimuKiekis": "10",
    "ivetinimoProcentas": "20%",
    "maxIvertinimas": "10",
    "minIvertinimas": "1",
},{
    "id": "2",
    "pavadinimas": "test",
    "terminas": "2020-12-20",
    "ikelimuKiekis": "10",
    "ivetinimoProcentas": "20%",
    "maxIvertinimas": "10",
    "minIvertinimas": "1",
}]

let studentai = [{
    "taskID": "1" ,
    "id": "20153051",
    "Vardas": "vardas",
    "Pavarde": "pavarde",
    "Ikelimo_data": "2014-12-14",
},{
    "taskID": "1" ,
    "id": "20153051",
    "Vardas": "vardas",
    "Pavarde": "pavarde",
    "Ikelimo_data": "2014-12-14",
}]

app.use(cors())
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/', function(req, res) {
    res.render('pages/index');
});

app.get('/api/uzduotys', (req, res) =>{
    if(req.device.type.toUpperCase() != 'PHONE'){
        res.render('pages/tasks', {tasklist})
        console.log(tasklist)
    }
    else {
        res.json({tasklist})
        console.log(tasklist)
    }
})
app.get('/api/uzduotysm', (req, res) =>{
        res.json({tasklist})
        console.log(tasklist)
})


app.get('/api/uzduotys/:id', (req, res) => {
    const id = req.params.id
    let task=[]
    for (task of tasklist) {
        if (task.id === id) 
            console.log(task)
    }
    if(task=== null){
        // sending 404 when not found something is a good practice
        res.status(404).send('Book not found');
    }
    else if(req.device.type.toUpperCase() != 'PHONE'){
        res.render('pages/task', {task})
        console.log(task)
    }
    else {
        res.json(task)
        console.log(task)
    }
});

app.get('/api/studentai/:id', (req, res) =>{
    const id = req.params.id
    let stud=[]
    var responce=[]
    for(stud of studentai){
        if (stud.taskID === id) {
            responce.push(stud)
        }
    }
    console.log(responce)
    if(responce=== null){
        // sending 404 when not found something is a good practice
        res.status(404).send('task not found');
    }
    else{
        res.render('pages/studnet', {responce})
    }

})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))